#include "image.h"
#include "readstat.h"
#include "writestat.h"
long calc_padding(const uint32_t width);
enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp(FILE* out, struct image const* img);
