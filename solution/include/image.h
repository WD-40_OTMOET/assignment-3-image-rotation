#pragma once
#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>

struct pixel {
	uint8_t b;
	uint8_t r;
	uint8_t g;
};

struct image {
  uint32_t width, height;
  struct pixel** data;
};

struct image new_image(const uint32_t width, const uint32_t height);
void free_image(struct image* img);
