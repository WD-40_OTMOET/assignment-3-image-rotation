#include "bmp.h"
#include "readstat.h"

enum read_status read_bmp(FILE* in, struct bmp_header* header);
