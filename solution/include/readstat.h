#pragma once

enum read_status  {
  READ_OK = 0,
  READ_INVALID_NULL_INPUT,
  READ_INVALID_FILE_FORMAT, 
  READ_FAILURE
};
