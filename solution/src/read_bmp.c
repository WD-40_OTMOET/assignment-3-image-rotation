#include <stdio.h>

#include "bmp.h"
#include "readstat.h"

enum read_status read_bmp(FILE* in, struct bmp_header* header){
  if (in == NULL){
    return READ_INVALID_NULL_INPUT;
  }
  size_t read = fread(header, sizeof(struct bmp_header), 1, in);
  if (read == 0){
		return READ_FAILURE;
  }
  if (header->bfType[0] != 'B' && header->bfType[1] != 'M'){
    return READ_INVALID_FILE_FORMAT;
  } else {
    return READ_OK;
  }
}
