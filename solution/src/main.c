#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"

#include "convert.h"
#include "read.h"
#include "rotate.h"


int main(int args, char** argv){
	if (args != 3) return 0;
  struct bmp_header header;
  FILE* in = fopen(argv[1], "rb");
  enum read_status read_result = read_bmp(in, &header);
  if (read_result != READ_OK){
    return 0;
  }
  fseek(in, header.bOffBits, SEEK_SET);

  struct image img = new_image(header.biWidth, header.biHeight);
  if (img.data == NULL){
  	return 0;
  }
  enum read_status from_bmp_result = from_bmp(in, &img);
  if (from_bmp_result != READ_OK){
    return 0;
  }
  fclose(in);

  struct bmp_header new_header = header;
  struct image new = rotate(img);
  if (new.data == NULL) {
  	return 0;
  }
  free_image(&img);
  new_header.biHeight = header.biWidth;
  new_header.biWidth = header.biHeight;
  
  FILE* out = fopen(argv[2], "w");
  fwrite(&new_header, sizeof(struct bmp_header), 1, out);
  to_bmp(out, &new);
  fclose(out);
  free_image(&new);

  return 0;
}
