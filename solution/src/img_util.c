#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

struct image new_image(const uint32_t width, const uint32_t height){
  struct pixel** data = malloc(height * sizeof(struct pixel*));
  if (data != NULL) {
  	for (uint32_t i = 0 ; i < height; i++){
    	data[i] = malloc(width * sizeof(struct pixel));
  	}
  }
  return (struct image) {.data = data, .width = width, .height = height};
}

void free_image(struct image* img){
  for (uint32_t i = 0; i < img->height; i++){
    free(img->data[i]);
  }
  free(img->data);
}
