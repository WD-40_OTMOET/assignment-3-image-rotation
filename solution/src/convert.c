#include <inttypes.h>
#include <stdio.h>

#include "image.h"
#include "readstat.h"
#include "writestat.h"

long calc_padding(const uint32_t width){
   return (4-((3*(long)width)%4))%4;
}

enum read_status from_bmp( FILE* in, struct image* img ){
  if (in == NULL){
  	return READ_INVALID_NULL_INPUT;
  }
  for (uint32_t i = img->height - 1; i >= 0 && i < img->height; i--){
    size_t read = fread(img->data[i], sizeof(struct pixel), img->width, in);
    if (read == 0){
    	return READ_FAILURE;
    }
    fseek(in, calc_padding(img->width), SEEK_CUR);
  }
  return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
  if (out == NULL || img == NULL){
    return WRITE_ERROR;
  }
  for (uint32_t i = img->height - 1; i >= 0 && i < img->height  ; i--){
    size_t wrote = fwrite(img->data[i], img->width, sizeof(struct pixel), out);
    if (wrote == 0){
    	return WRITE_ERROR;
    }
    fseek(out, calc_padding(img->width), SEEK_CUR);
  }
  return WRITE_OK;
}

