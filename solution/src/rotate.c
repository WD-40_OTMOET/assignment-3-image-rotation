#include "image.h"

struct image rotate( struct image const source ){
  struct image new = new_image( source.height, source.width);
  if (new.data != NULL) {
  	for (uint32_t i = 0; i < new.height; i++){
    	for (uint32_t j = 0; j < new.width; j++){
      	new.data[i][j] = source.data[j][source.width - i - 1];
    	}
  	}
  }
  return new;
}
